﻿using System;
class Building
{
    public int Floors;
    public int Area;
    public int Occupants;
}

class UseBuilding
{
    static void Main()
    {

        Building house = new Building();
        Building office = new Building();

        int areaPP;

        house.Occupants = 4;
        house.Area = 20;
        house.Floors = 1;

        office.Occupants = 50;
        office.Area = 500;
        office.Floors = 2;

        Console.WriteLine("Area per person for house is " + house.Area / house.Occupants);
        Console.WriteLine("Area per person for office is " + office.Area / office.Occupants);


    }
}